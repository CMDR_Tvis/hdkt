import java.net.URL

plugins { id("org.jetbrains.dokka"); kotlin("jvm"); `maven-publish` }
val holographicDisplaysApiVersion: String by project
val holographicDisplaysKTVersion: String by project
val kotlinApiVersion: String by project
val kotlinJvmTarget: String by project
val kotlinLanguageVersion: String by project
val kotlinVersion: String by project
val pluginApiVersion: String by project
val spigotApiVersion: String by project
description = "An awesome library, which a bit adapts Holographic Displays API for Kotlin."
group = "io.github.commandertvis"
version = holographicDisplaysKTVersion

repositories {
    jcenter()
    maven("https://gitlab.com/api/v4/projects/10077943/packages/maven")
    maven("https://hub.spigotmc.org/nexus/content/repositories/snapshots")
    maven("https://oss.sonatype.org/content/groups/public/")
    maven("https://repo.codemc.org/repository/maven-public/")
}

dependencies {
    fun pluginApi(module: String) = "io.github.commandertvis.plugin:$module:$pluginApiVersion"
    compileOnly("com.gmail.filoghost.holographicdisplays:holographicdisplays-api:$holographicDisplaysApiVersion")
    compileOnly("org.spigotmc:spigot-api:$spigotApiVersion")
    compileOnly(kotlin("stdlib-jdk8", kotlinVersion))
    compileOnly(pluginApi("common"))
    compileOnly(kotlin("stdlib-jdk8"))
}

tasks {
    compileKotlin.get().kotlinOptions {
        apiVersion = kotlinApiVersion
        freeCompilerArgs = listOf("-Xuse-experimental=kotlin.Experimental")
        jvmTarget = kotlinJvmTarget
        languageVersion = kotlinLanguageVersion
    }

    create<Copy>("copyToRun") { from(jar); into("/run/plugins/") }
    create<Jar>("sourcesJar") { from(sourceSets.main.get().allSource); archiveClassifier.set("sources") }

    dokka {
        outputDirectory = "public/"

        configuration {
            includes = listOf("PACKAGES.md")
            jdkVersion = 8
            moduleName = "main"
            platform = "JVM"
            targets = listOf("JVM")

            arrayOf(
                "https://ci.md-5.net/job/BungeeCord/ws/chat/target/apidocs/",
                "http://fasterxml.github.io/jackson-databind/javadoc/2.9/",
                "https://hub.spigotmc.org/javadocs/spigot/",
                /*"https://ci.codemc.io/job/filoghost/job/HolographicDisplays/javadoc/",*/
                "https://static.javadoc.io/com.google.code.gson/gson/2.8.5/"/*,
                "https://cmdr_tvis.gitlab.io/plugin-api/main/"*/
            ).forEach {
                externalDocumentationLink {
                    packageListUrl = URL("${it}package-list")
                    url = URL(it)
                }
            }

            perPackageOption { prefix = "kotlin" }

            sourceLink {
                path = "src/main/kotlin"
                url = "https://gitlab.com/CMDR_Tvis/hdkt/tree/master/src/main/kotlin/"
            }
        }
    }

    processResources.get().expand("description" to description, "pluginName" to "HDKT", "version" to version)
}

publishing {
    publications {
        create<MavenPublication>("mavenJava") {
            from(components["java"])
            artifact(tasks["sourcesJar"])

            pom {
                operator fun <T> Property<T>.invoke(value: T) = set(value)
                operator fun <T> SetProperty<T>.invoke(vararg values: T) = set(values.toList())
                packaging = "jar"
                name(project.name)
                description(project.description)
                url("https://gitlab.com/CMDR_Tvis/hdkt")
                inceptionYear("2019")

                developers {
                    developer {
                        email("postovalovya@gmail.com")
                        id("CMDR_Tvis")
                        name("Commander Tvis")
                        roles("architect", "developer")
                        timezone("7")
                        url("https://gitlab.com/CMDR_Tvis")
                    }
                }

                licenses {
                    license {
                        comments("Open-source license")
                        distribution("repo")
                        name("MIT License")
                        url("https://gitlab.com/CMDR_Tvis/hdkt/blob/master/LICENSE")
                    }
                }

                scm {
                    connection("scm:git:git@gitlab.com:CMDR_Tvis/hdkt.git")
                    developerConnection("scm:git:git@gitlab.com:CMDR_Tvis/hdkt.git")
                    url("git@gitlab.com:CMDR_Tvis/hdkt.git")
                }
            }
        }

        repositories {
            maven("https://gitlab.com/api/v4/projects/15076805/packages/maven") {
                credentials(HttpHeaderCredentials::class) {
                    name = "Job-Token"
                    value = System.getenv("CI_JOB_TOKEN")
                }

                authentication { register("header", HttpHeaderAuthentication::class) }
            }
        }
    }
}
