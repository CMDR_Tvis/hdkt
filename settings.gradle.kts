rootProject.name = "holographic-displays-kt"
val dokkaVersion: String by settings
val kotlinVersion: String by settings
pluginManagement { plugins { id("org.jetbrains.dokka") version dokkaVersion; kotlin("jvm") version kotlinVersion } }
