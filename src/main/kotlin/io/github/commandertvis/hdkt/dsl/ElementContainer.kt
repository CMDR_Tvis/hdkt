package io.github.commandertvis.hdkt.dsl

import com.gmail.filoghost.holographicdisplays.api.handler.PickupHandler
import com.gmail.filoghost.holographicdisplays.api.handler.TouchHandler
import io.github.commandertvis.hdkt.elements.HologramElement

public interface ElementContainer {
    public val pickupHandler: PickupHandler?
        get() = null

    public val touchHandler: TouchHandler?
        get() = null

    public fun toElement(): HologramElement<*>
}
