package io.github.commandertvis.hdkt.dsl

@DslMarker
@Retention(AnnotationRetention.BINARY)
@Target(AnnotationTarget.CLASS, AnnotationTarget.VALUE_PARAMETER)
public annotation class HologramsDslMarker public constructor()
