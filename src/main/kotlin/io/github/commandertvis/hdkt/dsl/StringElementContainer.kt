package io.github.commandertvis.hdkt.dsl

import com.gmail.filoghost.holographicdisplays.api.handler.TouchHandler
import io.github.commandertvis.hdkt.elements.StringElement
import io.github.commandertvis.hdkt.elements.toHologramElement
import org.bukkit.entity.Player

@HologramsDslMarker
public data class StringElementContainer public constructor(
    public var text: String = "",
    public override var touchHandler: TouchHandler? = null
) : ElementContainer {
    public override fun toElement(): StringElement = text.toHologramElement()
    public fun onTouch(handler: Player.() -> Unit): Unit = run { touchHandler = TouchHandler(handler) }
}
