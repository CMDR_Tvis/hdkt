package io.github.commandertvis.hdkt.dsl

import com.gmail.filoghost.holographicdisplays.api.handler.PickupHandler
import com.gmail.filoghost.holographicdisplays.api.handler.TouchHandler
import io.github.commandertvis.hdkt.elements.ItemStackElement
import io.github.commandertvis.hdkt.elements.toHologramElement
import org.bukkit.Material
import org.bukkit.entity.Player
import org.bukkit.inventory.ItemStack

@HologramsDslMarker
public data class ItemElementContainer public constructor(
    public val item: ItemStack = ItemStack(Material.AIR),
    public override var touchHandler: TouchHandler? = null,
    public override var pickupHandler: PickupHandler? = null
) : ElementContainer {
    public override fun toElement(): ItemStackElement = item.toHologramElement()
    public fun onTouch(handler: Player.() -> Unit): Unit = run { touchHandler = TouchHandler(handler) }
    public fun onPickup(handler: Player.() -> Unit): Unit = run { pickupHandler = PickupHandler(handler) }
}
