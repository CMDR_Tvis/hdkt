package io.github.commandertvis.hdkt.dsl

import com.gmail.filoghost.holographicdisplays.api.Hologram
import com.gmail.filoghost.holographicdisplays.api.HologramsAPI
import com.gmail.filoghost.holographicdisplays.api.line.CollectableLine
import com.gmail.filoghost.holographicdisplays.api.line.TouchableLine
import io.github.commandertvis.hdkt.appendElement
import io.github.commandertvis.hdkt.registerPlaceholder
import org.bukkit.Location
import org.bukkit.plugin.Plugin

@HologramsDslMarker
public data class HologramScope(
    public val plugin: Plugin,
    public val location: Location,
    private val _containers: MutableList<ElementContainer> = mutableListOf()
) {
    public val containers: List<ElementContainer>
        get() = _containers

    public var allowsPlaceholders: Boolean = true

    public fun placeholder(placeholder: String, refreshRate: Double, value: () -> Any?): Boolean =
        plugin.registerPlaceholder(placeholder, refreshRate, value)

    public fun string(block: StringElementContainer.() -> Unit) {
        _containers += StringElementContainer().apply(block)
    }

    public fun item(block: ItemElementContainer.() -> Unit) {
        _containers += ItemElementContainer().apply(block)
    }

    internal fun toHologram(): Hologram = HologramsAPI.createHologram(plugin, location)!!.also { hologram ->
        hologram.isAllowPlaceholders = allowsPlaceholders

        _containers.forEach { container ->
            hologram.appendElement(container.toElement()).also { line ->
                if (line is TouchableLine)
                    line.touchHandler = container.touchHandler

                if (line is CollectableLine)
                    line.pickupHandler = container.pickupHandler
            }
        }
    }
}
