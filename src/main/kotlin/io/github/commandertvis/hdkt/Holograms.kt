package io.github.commandertvis.hdkt

import com.gmail.filoghost.holographicdisplays.api.Hologram
import com.gmail.filoghost.holographicdisplays.api.line.HologramLine
import io.github.commandertvis.hdkt.elements.HologramElement

public fun Hologram.appendElement(element: HologramElement<*>): HologramLine = element.applyTo(hologram = this)

public fun Hologram.appendElements(elements: Iterable<HologramElement<*>>): List<HologramLine> =
    elements.map(this::appendElement)

public fun Hologram.appendElements(elements: Sequence<HologramElement<*>>): Sequence<HologramLine> =
    elements.map(this::appendElement)

public fun Hologram.appendElements(vararg elements: HologramElement<*>): List<HologramLine> =
    elements.map(this::appendElement)

public fun Hologram.changeElements(elements: Iterable<HologramElement<*>>): List<HologramLine> {
    clearLines()
    return appendElements(elements)
}

public fun Hologram.changeElements(elements: Sequence<HologramElement<*>>): Sequence<HologramLine> {
    clearLines()
    return appendElements(elements)
}

public fun Hologram.changeElements(vararg elements: HologramElement<*>): List<HologramLine> {
    clearLines()
    return appendElements(*elements)
}
