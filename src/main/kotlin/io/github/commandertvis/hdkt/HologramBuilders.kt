package io.github.commandertvis.hdkt

import com.gmail.filoghost.holographicdisplays.api.Hologram
import io.github.commandertvis.hdkt.dsl.HologramScope
import org.bukkit.Location
import org.bukkit.plugin.Plugin
import kotlin.contracts.ExperimentalContracts
import kotlin.contracts.InvocationKind
import kotlin.contracts.contract

@UseExperimental(ExperimentalContracts::class)
public fun Plugin.hologram(location: Location, block: HologramScope.() -> Unit): Hologram {
    contract { callsInPlace(block, InvocationKind.EXACTLY_ONCE) }
    return HologramScope(plugin = this, location = location).apply(block).toHologram()
}

@UseExperimental(ExperimentalContracts::class)
public fun Plugin.changeHologram(hologram: Hologram, block: HologramScope.() -> Unit): Hologram {
    contract { callsInPlace(block, InvocationKind.EXACTLY_ONCE) }

    return hologram(hologram.location) {
        allowsPlaceholders = hologram.isAllowPlaceholders
        hologram.visibilityManager
        block()
    }
}
