package io.github.commandertvis.hdkt.elements

import com.gmail.filoghost.holographicdisplays.api.Hologram
import com.gmail.filoghost.holographicdisplays.api.line.HologramLine
import com.gmail.filoghost.holographicdisplays.api.line.ItemLine
import com.gmail.filoghost.holographicdisplays.api.line.TextLine
import org.bukkit.inventory.ItemStack

public fun String.toHologramElement(): StringElement =
    StringElement(value = this)
public fun ItemStack.toHologramElement(): ItemStackElement =
    ItemStackElement(value = this)

public sealed class HologramElement<T> {
    public abstract val value: T
    public abstract fun applyTo(hologram: Hologram): HologramLine
}

public open class ItemStackElement(public override val value: ItemStack) : HologramElement<ItemStack>() {
    public override fun applyTo(hologram: Hologram): ItemLine = hologram.appendItemLine(value)
}

public open class StringElement(public override val value: String) : HologramElement<String>() {
    public override fun applyTo(hologram: Hologram): TextLine = hologram.appendTextLine(value)
}
