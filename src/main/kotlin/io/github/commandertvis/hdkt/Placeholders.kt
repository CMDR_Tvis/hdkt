package io.github.commandertvis.hdkt

import com.gmail.filoghost.holographicdisplays.api.HologramsAPI
import org.bukkit.plugin.Plugin
import kotlin.contracts.ExperimentalContracts
import kotlin.contracts.InvocationKind
import kotlin.contracts.contract

public val Plugin.registeredPlaceholders: Collection<String>
    get() = HologramsAPI.getRegisteredPlaceholders(this)

@UseExperimental(ExperimentalContracts::class)
public fun Plugin.registerPlaceholder(placeholder: String, refreshRate: Double, value: () -> Any?): Boolean {
    contract { callsInPlace(value, InvocationKind.UNKNOWN) }
    return HologramsAPI.registerPlaceholder(this, placeholder, refreshRate) { value().toString() }
}

public fun Plugin.unregisterPlaceholders(): Unit = HologramsAPI.unregisterPlaceholders(this)

public fun Plugin.unregisterPlaceholder(placeholder: String): Boolean =
    HologramsAPI.unregisterPlaceholder(this, placeholder)
